import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './error404.component';
import { HomeComponent } from './home.component';


const routes: Routes = [
  { path: "angular/lespersonnes", loadChildren: "./personne/personne.module#PersonneModule" },
  { path: "angular/lescomptes", loadChildren: "./compte/compte.module#CompteModule" },
  { path: "angular/auth", loadChildren: "./auth/auth.module#AuthModule" },
  { path: "angular/home", component: HomeComponent },
  { path: "", redirectTo: "angular/home", pathMatch: "full"},
  { path: "**", component: Error404Component }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}