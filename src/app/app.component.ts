import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonneService } from './personne/personne.service';
import { AuthService } from './shared/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'cb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isConnected: boolean;

  private subscription: Subscription;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.subscription = this.authService.jwtToken.subscribe( token => {
      this.isConnected = token.isAuthenticated;

      console.log(this.isConnected);
    });
  }

  /**
   * Appel service auth de déconnexion
   */
  public logout() {
    this.authService.logout();
    // redirection page login
    this.router.navigate(['/angular/auth']);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
