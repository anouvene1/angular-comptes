import { NgModule } from '@angular/core';
import { RouterModule, Route, Routes } from '@angular/router';
import { SigninComponent } from './signin/signin.component';

const ROUTES: Routes = [
    { 
        path: "", component: SigninComponent,
        children: []
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {

}