import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cb-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  error: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signin(val: any) {
    this.authService.signin(val).subscribe(
      (jwt) => {
        console.log(jwt);
        if(jwt !== null) {
          this.router.navigate(['/angular/lespersonnes/client/1']);
        }
      },
      (err) => {
        this.error = err.error;
        this.router.navigate(['/angular/auth']);
      }
    );
  }

}
