import { Component, OnInit, Input } from '@angular/core';
import { CompteService } from './compte.service';
import { CompteModel } from './compte.model';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';


@Component({
  selector: 'cb-add-compte',
  templateUrl: './add-compte.component.html',
  styleUrls: ['./add-compte.component.css']
})
export class AddCompteComponent implements OnInit {
  id: number;

  currentCompte: CompteModel;

  constructor(private compteService: CompteService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
   
    this.route.paramMap.subscribe( (params: ParamMap) => {
      this.id = parseInt(params.get("id"));
    });

    if(!isNaN(this.id)){
      this.compteService.retreiveCompteById(this.id).subscribe( (compte: CompteModel) => {
        this.currentCompte = compte;
      });
    }


    
  }

  addCompte(compte:CompteModel) {
    console.log("Form values: ", compte);
    this.compteService.createCompte(compte).subscribe( res => console.log(res));
  }

  modifyCompte(id: number, compte:CompteModel) {
    this.compteService.updateCompte(this.id, compte).subscribe( res => {
      console.log(res);
      this.router.navigate(['/angular/lescomptes']);
    });
  }



}
