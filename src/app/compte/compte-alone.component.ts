import { Component, OnInit, Input } from '@angular/core';
import { CompteModel } from './compte.model';
import { CompteService } from './compte.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'cb-compte-alone',
  templateUrl: './compte-alone.component.html',
  styleUrls: ['./compte-alone.component.css']
})
export class CompteAloneComponent implements OnInit {
  @Input("compteToUpdate")
  compte: CompteModel;
  
  constructor(private compteService: CompteService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {}

  removeCompte(id: number) {
    this.compteService.deleteCompte(id).subscribe( res => {
      this.router.navigate(['/angular/lescomptes']);
    });
  }

}
