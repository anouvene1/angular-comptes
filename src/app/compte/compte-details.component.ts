import { Component, OnInit } from '@angular/core';
import { CompteService } from './compte.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { CompteModel } from './compte.model';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CompteModule } from './compte.module';

@Component({
  selector: 'cb-compte-details',
  templateUrl: './compte-details.component.html',
  styleUrls: ['./compte-details.component.css']
})
export class CompteDetailsComponent implements OnInit {
  public compte$: Observable<CompteModel>;
  public id: number;

  constructor(private compteService: CompteService, private route: ActivatedRoute) { }

  ngOnInit() {
    
    this.route.paramMap.subscribe( (params: ParamMap) => {
      this.id = +params.get("id");

      // Récupérer un compte by id
      this.compte$ = this.compteService.retreiveCompteById(this.id);
    });

  }

  

}
