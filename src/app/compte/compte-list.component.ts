import { Component, OnInit } from '@angular/core';
import { CompteModel } from './compte.model';
import { CompteService } from './compte.service';

@Component({
  selector: 'cb-compte-list',
  templateUrl: './compte-list.component.html',
  styleUrls: ['./compte-list.component.css']
})
export class CompteListComponent implements OnInit {

  comptes: CompteModel[] = [];
  constructor(private compteService: CompteService) {}

  ngOnInit() {
    this.compteService.retrieveComptes().subscribe( 
      (lesComptes: CompteModel[]) => {
      this.comptes = lesComptes;
    });
     
  }

}
