import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompteListComponent } from './compte-list.component';
import { CompteDetailsComponent } from './compte-details.component';
import { AddCompteComponent } from './add-compte.component';

const ROUTES: Routes = [
    {
        path: "add-compte", component: AddCompteComponent
    },
    {
        path: "add-compte/:id", component: AddCompteComponent
    },
    { 
        path: "", component: CompteListComponent,
        
        children: [
            { path: ":id", component: CompteDetailsComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class CompteRoutingModule { }