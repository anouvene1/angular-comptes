import {FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';


import { CompteListComponent } from './compte-list.component';
import { CompteRoutingModule } from './compte-routing.module';
import { CompteDetailsComponent } from './compte-details.component';
import { AddCompteComponent } from './add-compte.component';

import { MaterialModule } from '../material/material.module';
import { CompteAloneComponent } from './compte-alone.component';


@NgModule({
  declarations: [
    CompteListComponent,
    CompteDetailsComponent,
    AddCompteComponent,
    CompteAloneComponent
  ],
  imports: [
    CommonModule,
    CompteRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
  ],
  exports: [
  ]
})
export class CompteModule { }
