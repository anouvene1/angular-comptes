import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CompteModel } from './compte.model';
import { Observable, Subject, BehaviorSubject, throwError } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CompteService implements OnInit {

  // private idCompte: number;

  constructor(private httpClient: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.route.paramMap.subscribe( (params: ParamMap) => {
    //   this.idCompte = +params.get("id");
    // });
  }

  // GET
  retrieveComptes(): Observable<CompteModel[]> {
    // const headers = new HttpHeaders({'Content-Type':'application/json', 'Access-Control-Allow-Origin':'*'});
    // return this.httpClient.get<CompteModel[]>("/spring/lescomptes", {headers: headers});
    return this.httpClient.get<CompteModel[]>("/api/lescomptes");
  }

  retreiveCompteById(id: number): Observable<any> {
    return this.httpClient.get<CompteModel>("/api/lescomptes/" + id);
  }

  // POST
  createCompte(compte: CompteModel): Observable<CompteModel> {
    
    console.log("Compte to create: ", compte);

    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post<CompteModel>("/api/lescomptes/", JSON.stringify(compte), options);
  }

  // PUT
  updateCompte(id: number, compte: CompteModel): Observable<CompteModel> {
    
    console.log("Compte to update: ", compte);

    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.put<CompteModel>("/api/lescomptes/" + id, JSON.stringify(compte), options);
  }

  // DELETE
  deleteCompte(id: number) {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.delete<CompteModel>("/api/lescomptes/" + id, options);
  }

  // Error handling
  errorHandl(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     console.log(errorMessage);
     return throwError(errorMessage);
  }

}
