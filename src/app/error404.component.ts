import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cb-error404',
  template: `
    <h3>oops!</h3>
    <p>Page not found</p>
  `,
  styles: ['h3 {color: red}']
})
export class Error404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
