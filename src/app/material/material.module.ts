import { NgModule } from '@angular/core';
import { 
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatCheckboxModule,
  MatRadioModule,
  MatButtonModule,
  MatCardModule
} from "@angular/material";

const Material = [
  MatFormFieldModule,
  MatSelectModule,
  MatInputModule,
  MatCheckboxModule,
  MatRadioModule,
  MatButtonModule, 
  MatCardModule
];

@NgModule({
  declarations: [],
  imports: [
    ...Material   
  ],
  exports: Material
})
export class MaterialModule { }