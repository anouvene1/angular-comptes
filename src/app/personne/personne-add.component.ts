import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { PaysService } from '../shared/services/pays.service';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaysModel } from '../shared/models/pays.model';
import { VilleModel } from '../shared/models/ville.model';
import { VilleService } from '../shared/services/ville.service';
import { PersonneService } from './personne.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'cb-personne-add',
  templateUrl: './personne-add.component.html',
  styleUrls: ['./personne-add.component.css']
})
export class PersonneAddComponent implements OnInit, OnDestroy {
  lespays$: Observable<PaysModel[]>;
  lesvilles$: Observable<VilleModel[]>;

  notif: string;
  subscription: Subscription;

  erreursForm = {
    'nom': '',
    'email': ''
  };

  messagesErreur = {
    'nom': {
      'required': 'Ce champ est requis.',
      'minlength': 'Vos nom et prénom doivent faire au moins 4 caractères.',
      'isTuan': 'Ce nom existe déjà !!'
    },
    'email': {
      'required': 'Entrez un email.',
      'isEmailValid': "Rentrez une adresse email valide.",
      'isAsyncEmailValid': "L'email n'existe pas."
    }
  };

  addPersonForm: FormGroup;

  constructor(
    private paysService: PaysService, 
    private villeService: VilleService, 
    private fb: FormBuilder,
    private personneService: PersonneService,
    private http: HttpClient) { }

  ngOnInit() {
    this.lespays$ = this.paysService.retreiveAllPays();

    // Méthode classique
    // this.addPersonForm = new FormGroup({
    //   nom: new FormControl('', Validators.required),
    //   tel: new FormControl(''),
    //   email: new FormControl('', Validators.required),
    //   adresse: new FormControl('',  Validators.required),
    //   cp: new FormControl('',  Validators.required),
    //   pays: new FormControl('',  Validators.required),
    //   ville: new FormControl('',  Validators.required)
    // });

    // Avec FormBuilder
    this.addPersonForm = this.fb.group({
      nom: this.fb.control('', [Validators.required, this.nomValidator()]),
      tel: this.fb.control(''),
      email: this.fb.control('', [Validators.required, this.emailValidator()]/*, this.asyncEmailValidator()*/),
      adresse: this.fb.control('',  Validators.required),
      cp: this.fb.control('',  Validators.required),
      ville: this.fb.control('',  Validators.required),
      pays: this.fb.control('',  Validators.required),
    });

    // Suivre l'état du formulaire valid ou invalid
    this.subscription = this.addPersonForm.statusChanges
      .subscribe(() => this.changementStatusForm());

    this.onChanges();
  }

  /**
   * nom Validator
   */
  nomValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      return (control.value === "tuan") ? { isTuan: true } : null;
    }
  }

  /**
   * email synchrone validator
   */
  emailValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return (control.value && !control.value.match(/^[\w.]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) ?
        { isEmailValid: control.value } : null;
    }
  }

  /**
   * email asynchrone validator
   */
  asyncEmailValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.http.get(`https://apilayer.net/api/check?access_key=115ce3ae2ccdef30e018edbde78d2c4a&email=${control.value}&smtp=1&format=1`)
        .pipe(
          map((response: any) => {
            return !response.smtp_check ? { isAsyncEmailValid: control.value } : null;
          }))
    }
  }

  /**
   * Treatment errors messages fields
   */
  changementStatusForm() {
    if (!this.addPersonForm) { return; }

    const form = this.addPersonForm;
    for (const field in this.erreursForm) {
      this.erreursForm[field] = '';

      const control = form.get(field);
      if (control && control.dirty && control.invalid) {
        const messages = this.messagesErreur[field];
        for (const key in control.errors) {
          this.erreursForm[field] += messages[key] + ' ';
        }
      }
    }
  }

  /**
   * Get Pays and Ville
   */
  public onChanges() {
    this.addPersonForm.get('pays').valueChanges.subscribe( pays => {
      const idPays: number = +pays.id;

      if(idPays > 0) {
        this.lesvilles$ = this.villeService.retreiveAllVilleByPays(idPays);
      }
    });
  }

  /**
   * Add Personne
   * @param formValue Personne form data 
   */
  public addPerson(formValue: any) {
    console.log(formValue);

    this.personneService.registerPerson(formValue).subscribe( 
      res => {
        if(res !== null) {
          this.notif = "Enregistrement réussi !!";
        }
      },
      error => this.notif = "problème server !!"
    );
  }

  ngOnDestroy(): void {
    //throw new Error("Method not implemented.");
    this.subscription.unsubscribe();
  }


}
