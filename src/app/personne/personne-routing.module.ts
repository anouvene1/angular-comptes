import { NgModule } from '@angular/core';
import { PersonnesComponent } from './personnes.component';
import { RouterModule, Route, Routes } from '@angular/router';
import { PersonneAddComponent } from './personne-add.component';
import { PersonneComponent } from './personne.component';
import { AuthGuard } from '../shared/guards/auth.guard';

const ROUTES: Routes = [
    { 
        path: "", component: PersonnesComponent,
        children: []
    },
    { path: "add-client", canActivate:[AuthGuard], component: PersonneAddComponent },
    { path: "client/1", canActivate:[AuthGuard], component: PersonneComponent }

];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
        RouterModule
    ]
})
export class PersonneRoutingModule {

}