import { VilleModel } from '../shared/models/ville.model';
import { PaysModel } from '../shared/models/pays.model';

export class PersonneModel {
    id: number;
    adresse: string;
    cp: string;
    email: string;
    nom: string;
    tel: string;
    ville: VilleModel;
    pays: PaysModel;
}