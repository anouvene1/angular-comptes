import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonneComponent } from './personne.component';
import { PersonnesComponent } from './personnes.component';
import { PersonneRoutingModule } from './personne-routing.module';
import { PersonneAddComponent } from './personne-add.component';
import { MaterialModule } from '../material/material.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    PersonneComponent, 
    PersonnesComponent, 
    PersonneAddComponent
  ],
  imports: [
    CommonModule,
    PersonneRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class PersonneModule { }
