import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonneModel } from './personne.model';
import { PaysModel } from '../shared/models/pays.model';
import { VilleModel } from '../shared/models/ville.model';

@Injectable({
  providedIn: 'root'
})
export class PersonneService implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit(): void {}

  /**
   * Create a Person
   * @param person Persone to register
   */
  public registerPerson(person: PersonneModel): Observable<PersonneModel> {
    let options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }

    return this.http.post<PersonneModel>("/api/lespersonnes", JSON.stringify(person), options);
  }

  /**
   * Retreive all persons
   */
  public retreiveAllPersons() {
    return this.http.get<PersonneModel[]>("/api/lespersonnes");
  }

  /**
   * Persons by country
   * @param pays Country selected
   */
  public retreiveAllPersonsFromCountry(paysId: number): Observable<PersonneModel[]> {
    return this.http.get<PersonneModel[]>("/api/lespersonnes/pays/" + paysId);
  }

  /**
   * Persons by city
   * @param ville City selected
   */
  public retreiveAllPersonsFromCity(villeId: number): Observable<PersonneModel[]> {
    return this.http.get<PersonneModel[]>("/api/lespersonnes/ville/" + villeId);
  }







}
