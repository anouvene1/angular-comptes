import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { PersonneModel } from './personne.model';
import { PersonneService } from './personne.service';
import { VilleService } from '../shared/services/ville.service';
import { PaysService } from '../shared/services/pays.service';
import { PaysModel } from '../shared/models/pays.model';
import { VilleModel } from '../shared/models/ville.model';

@Component({
  selector: 'cb-personnes',
  templateUrl: './personnes.component.html',
  styleUrls: ['./personnes.component.css']
})
export class PersonnesComponent implements OnInit {
  villes$: Observable<VilleModel[]>;
  pays$: Observable<PaysModel[]>;
  personnes$: Observable<PersonneModel[]>;

  ville$: BehaviorSubject<VilleModel> = new BehaviorSubject(null);
  villeSelected: VilleModel;
  paysSelected: PaysModel;

  constructor(private villeService: VilleService, private paysService: PaysService, private personnesServices: PersonneService) { }

  ngOnInit() {
    this.villes$ = this.villeService.retreiveAllVilles();
    this.pays$ = this.paysService.retreiveAllPays();
    this.personnes$ = this.personnesServices.retreiveAllPersons();

    this.ville$.subscribe( ville => {
      console.log(ville);
      this.villeSelected = ville;
    });
  }

  filterPersons(ville: VilleModel) {
    console.log(ville.id);

    this.personnes$ = this.personnesServices.retreiveAllPersonsFromCity(ville.id);

  }

  villeChanged(event: any) {
    this.filterPersons(event.value);

    // this.ville$.next(event.value);

    // this.ville$.subscribe( ville => {
    //   this.filterPersons(ville);
    // });
    
  }

}
