import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';
import { JwtTokenModel } from '../models/Jwt-token.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}
  
  /**
   * Protection des routes pour les utilisateurs connectés
   * @param next RouteSnapshot
   * @param state Router
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      return this.authService.jwtToken.pipe(
        map( (token: JwtTokenModel) => {
          if (token.isAuthenticated) {
            return true;
          } else {
            this.router.navigate(['/angular/auth']);
            return false;
          }
        })
      );
  
  }
  
}
