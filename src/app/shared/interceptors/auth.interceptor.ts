import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

export class AuthInterceptor implements HttpInterceptor {
  /**
   * Intercepter une requête http entrante avant de l'envoyer
   * au serveur back-end
   * @param req Requête entrante
   * @param next Passer la main à la requête suivante
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = sessionStorage.getItem('jwt');
    if (token) { // Si le token existe
      // Alors clôner la requête entrante pour en faire une nouvelle requête
      const authReq = req.clone({
        // Puis dans la nouvelle requête, insérer le token dans son header authorization
        headers: req.headers.set('authorization', token)
      });
      // Retourner la nouvelle requête ainsi formée et passer la main à la requête suivante
      return next.handle(authReq);
    } else { // Sinon retourner simplement la requête initiale et passer la main à la suivante
      return next.handle(req);
    }
  }
}
