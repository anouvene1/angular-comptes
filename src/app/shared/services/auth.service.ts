import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription, Observable, timer, of } from 'rxjs';
import { JwtTokenModel } from '../models/Jwt-token.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PersonneModel } from 'src/app/personne/personne.model';
import { tap, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Token
  public jwtToken: BehaviorSubject<JwtTokenModel> = new BehaviorSubject({
    isAuthenticated: null,
    token: null
  });

  // Subscription to timer Observable
  public subscription: Subscription;

  /**
   * HttpClient
   * @param http HttpClient service
   */
  constructor(private http: HttpClient) {
    // this.initToken(); // Appel init token
    // this.subscription = this.initTimer(); // Initialisation du timer à l'instanciation du service
  }

  /**
   * Inscription
   * Poster signupForm
   * @param user UserModel
   */
  public signup(user: PersonneModel): Observable<PersonneModel> {
    return this.http.post<PersonneModel>('/api/auth/signup', user);
  }

  /**
   * Connexion
   * Récupérer le token et le conserver localement pendant toute la durée de la connexion
   * Poster signinForm
   * @param credentials Identifiants utilisateur
   */
  public signin(credentials: {username: string, password: string}): Observable<string> {
    let options: Object = {
      headers: new HttpHeaders({'content-type':'application/json'}),
      responseType: 'text'
    };

    return this.http.post<string>('/api/generatetoken', credentials, options).pipe(
      tap((token: string) => {
        this.jwtToken.next({
          isAuthenticated: true,
          token: token
        });
        sessionStorage.setItem('jwt', token); // stoker localement le token pour une durée limitée
      })
    );
  }

  /**
   * Déconnexion
   * Mettre à jour le jwtToken Subject
   * Vider le sessionStorage
   */
  public logout(): void {
    this.jwtToken.next({
      isAuthenticated: false,
      token: null
    });

    sessionStorage.removeItem('jwt');
  }

  /**
   * Initialiser le token
   * Permettre de déterminer si un utilisateur est connecté selon l'existence ou non du token
   */
  private initToken(): void {
    const token = sessionStorage.getItem('jwt'); // récupérer le token conservé en local

    if (token) {
      this.jwtToken.next({
        isAuthenticated: true,
        token: token
      });
    } else {
      this.jwtToken.next({
        isAuthenticated: false,
        token: null
      });
    }

  }

  /**
   * Initialiser un timer
   * Si un token existe en local, alors demander un nouveau au serveur back-end
   * Mettre à jour token BehaviorSubject à la derniere valueur retourné par le serveur
   * Puis stoker le nouveau token dans sessionStorage
   *
   * Si timer renvoie null (pas de token à renouveler), alors mettre jwtToken aux valeurs par defaut
   * Et vider sessionStorage
   *
   * Penser aussi à désinscrire le timer si pas de token à renouveler
   */
  private initTimer() {
    return timer(2000, 600000).pipe(
      switchMap(() => {
        if (sessionStorage.getItem('jwt')) {
          console.log('Try refresh the token');

          let options: Object = {
            headers: new HttpHeaders({'content-type':'application/json'}),
            responseType: 'text'
          };

          return this.http.post<string>('/api/validatetoken', {"token": sessionStorage.getItem('jwt')}, options).pipe(
            tap((token: string) => {
              this.jwtToken.next({
                isAuthenticated: true,
                token: token
              });
              sessionStorage.setItem('jwt', token);
            })
          );
        } else {
          console.log('No token to refresh !');

          this.subscription.unsubscribe(); // Désinscrire le timer
          return of(null);
        }
      })
    ).subscribe(
      () => {},
      err => {
        this.jwtToken.next({
          isAuthenticated: false,
          token: null
        });
        sessionStorage.removeItem('jwt');
        this.subscription.unsubscribe(); // Désinscrire le timer
      }
    );
  }

}
