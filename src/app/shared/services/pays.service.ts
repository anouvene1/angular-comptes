import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaysModel } from '../models/pays.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  constructor(private http: HttpClient) { }

  // GET Pays
  public retreiveAllPays(): Observable<PaysModel[]> {
    return this.http.get<PaysModel[]>("/api/lespays");
  }



}
