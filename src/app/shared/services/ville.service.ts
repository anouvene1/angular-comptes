import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VilleModel } from '../models/ville.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  constructor(private http: HttpClient) { }

  /**
   * Retreive all cities
   */
  public retreiveAllVilles(): Observable<VilleModel[]> {
    return this.http.get<VilleModel[]>("/api/lesvilles");
  }

  /**
   * Retreive cities by country id
   * @param idPays Country id
   */
  public retreiveAllVilleByPays(idPays: number): Observable<VilleModel[]> {
    return this.http.get<VilleModel[]>("/api/lesvilles/"+idPays);
  }
  
}
